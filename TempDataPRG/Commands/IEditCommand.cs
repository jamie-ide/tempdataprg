﻿using System.Web.Mvc;

namespace TempDataPRG.Commands
{
    public interface IEditCommand<T> where T : class 
    {
        // Gets the fully populated view model
        T Get(int id);

        // Validates the view model. Contract is that if validation fails, view model will be
        // restored to original state with any user changes so that it can be displayed on the edit page again.
        bool TryValidate(ModelStateDictionary modelState, T viewModel);

        // Performs the save -- does not validate, throws exception if save fails.
        void Save(T viewModel);

        // Combines TryValidate and Save
        bool TryValidateAndSave(ModelStateDictionary modelState, T viewModel);
    }
}