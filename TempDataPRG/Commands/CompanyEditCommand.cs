﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TempDataPRG.Models;
using TempDataPRG.ViewModels;

namespace TempDataPRG.Commands
{
    public class CompanyEditCommand : EditCommand<CompanyEditViewModel>
    {

        public CompanyEditCommand(HttpRequestBase request, TempDataDictionary tempData) : base(request, tempData)
        {}

        protected override CompanyEditViewModel GetViewModel(int id)
        {
            var company = Company.Companies().SingleOrDefault(x => x.CompanyId == id);
            if (company == null)
            {
                return null;
            }

            var vm = new CompanyEditViewModel
            {
                CompanyId = company.CompanyId,
                Name = company.Name,
                IndustryId = company.Industry.IndustryId,
                IndustriesList = Industry.Industries()
                    .Select(i => new SelectListItem()
                    {
                        Text = i.Name,
                        Value = i.IndustryId.ToString(),
                        Selected = i.IndustryId == company.Industry.IndustryId
                    })
            };

            return vm;
        }

        protected override bool TryValidateViewModel(ModelStateDictionary modelState, CompanyEditViewModel viewModel)
        {
            // custom validation goes here
            // testing
            if (viewModel.CompanyId == 1)
            {
                // Model (not property) level errors must have key = empty string
                modelState.AddModelError(string.Empty, "CompanyId 1 always has an error.");                
            }

            return modelState.IsValid;
        }

        protected override void RefreshViewModel(CompanyEditViewModel previous, CompanyEditViewModel current)
        {
            current.IndustriesList = previous.IndustriesList;
        }

        public override void Save(CompanyEditViewModel viewModel)
        {
            Console.WriteLine("Saved");
        }
    }
}