﻿using System.Web;
using System.Web.Mvc;

namespace TempDataPRG.Commands
{
    public abstract class EditCommand<T> : IEditCommand<T> where T : class
    {
        private readonly string _tempDataKey;
        private readonly TempDataDictionary _tempData;

        protected EditCommand(HttpRequestBase request, TempDataDictionary tempData)
        {
            var routeValues = request.RequestContext.RouteData.Values;
            // need a unique key to distinguish between requests; need to add tenant in Caboodle
            // might be worth checking the route name
            _tempDataKey = string.Format("{0}-{1}-{2}", routeValues["controller"], routeValues["action"], routeValues["id"]);
            _tempData = tempData;
        }

        private T PreviousViewModel
        {
            get { return (T)_tempData[_tempDataKey]; }
            set { _tempData[_tempDataKey] = value; }
        }

        /// <summary>
        /// Overrides return a fully populatad view model ready for editing.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected abstract T GetViewModel(int id);

        /// <summary>
        /// Overrides refresh the view model to restore any data that was not passed into the POST action.
        /// </summary>
        /// <param name="previous"></param>
        /// <param name="current"></param>
        protected abstract void RefreshViewModel(T previous, T current);

        /// <summary>
        /// Overrides provide the save implementation.
        /// </summary>
        /// <param name="viewModel"></param>
        public abstract void Save(T viewModel);

        /// <summary>
        /// The default implementaion simply returns ModelState.IsValid. Override if custom validation logic is required.
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        protected virtual bool TryValidateViewModel(ModelStateDictionary modelState, T viewModel)
        {
            return modelState.IsValid;
        }

        public T Get(int id)
        {
            var viewModel = GetViewModel(id);
            PreviousViewModel = viewModel;
            return viewModel;
        }

        public bool TryValidate(ModelStateDictionary modelState, T viewModel)
        {
            var result = TryValidateViewModel(modelState, viewModel);
            RefreshViewModel(PreviousViewModel, viewModel);
            PreviousViewModel = viewModel;
            return result;
        }

        public bool TryValidateAndSave(ModelStateDictionary modelState, T viewModel)
        {
            if (TryValidate(modelState, viewModel))
            {
                Save(viewModel);
                return true;
            }
            return false;
        }
    }
}