﻿using System.Collections.Generic;
using System.Linq;

namespace TempDataPRG.Models
{
    public class Company
    {
        private Company(int companyId, string name, Industry industry)
        {
            CompanyId = companyId;
            Name = name;
            Industry = industry;
        }

        public int CompanyId { get; private set; }
        public string Name { get; private set; }
        public Industry Industry { get; private set; }

        public static IEnumerable<Company> Companies()
        {
            return new List<Company>
            {
                new Company(1, "Ben & Jerry's", Industry.Industries().Single(x => x.IndustryId == 1)),
                new Company(2, "Maple Grove", Industry.Industries().Single(x => x.IndustryId == 2)),
                new Company(3, "Alchemist", Industry.Industries().Single(x => x.IndustryId == 3))
            };
        }
    }
}