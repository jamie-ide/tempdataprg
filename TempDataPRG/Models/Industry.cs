﻿using System.Collections.Generic;

namespace TempDataPRG.Models
{
    public class Industry
    {
        private Industry(int industryId, string name)
        {
            IndustryId = industryId;
            Name = name;
        }

        public int IndustryId { get; private set; }
        public string Name { get; protected set; }

        public static IEnumerable<Industry> Industries()
        {
            return new List<Industry>
            {
                new Industry(1, "Ice Cream"),
                new Industry(2, "Maple Syrup"),
                new Industry(3, "Beer")
            };
        }
        
    }
}