﻿using System.Web.Mvc;
using TempDataPRG.Commands;
using TempDataPRG.Models;
using TempDataPRG.ViewModels;

namespace TempDataPRG.Controllers
{
    public class CompaniesController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var vm = Company.Companies();
            return View(vm);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cmd = new CompanyEditCommand(Request, TempData);
            var vm = cmd.Get(id);
            if (vm == null)
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(CompanyEditViewModel vm)
        {
            var cmd = new CompanyEditCommand(Request, TempData);
            if (cmd.TryValidateAndSave(ModelState, vm))
            {
                return RedirectToAction("Index");
            }
            return View("Edit", vm);
        }
    }
}