﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace TempDataPRG.ViewModels
{
    public class CompanyEditViewModel
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }

        public int IndustryId { get; set; }
        public IEnumerable<SelectListItem> IndustriesList { get; set; }
    }
}